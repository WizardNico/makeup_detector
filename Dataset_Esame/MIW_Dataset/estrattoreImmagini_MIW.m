load('C:\Users\andre\Desktop\Dataset_Esame\MIW\Makeup_MIW.mat');
  
for i = 1:154
  image = reshape(MIW_matrix(:,i),[150 130 3]);
  
  if MIW_label(i) == 0
    image_descriptor = 'N';
   else
    image_descriptor = 'Y';
   end
   
  number=num2str(i);
  
  name_file = strcat(number, '_', image_descriptor, '.jpg');
  
  imwrite(image, name_file);
end