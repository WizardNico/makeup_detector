﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognition.Models
{
   public enum ImageType
   {
      ROI = 1,
      Face = 2,
      Gabor = 3,
      GIST = 4,
      Spectral = 5
   }
}
