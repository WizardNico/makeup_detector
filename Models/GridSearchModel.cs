﻿using BioLab.Common;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognition.Models
{
  public class GridSearchModel
  {
    public ConcurrentDictionary<string, FeatureVector> Roi_Makeup { get; set; }
    public ConcurrentDictionary<string, FeatureVector> Roi_NoMakeup { get; set; }
    public ConcurrentDictionary<string, FeatureVector> FaceMakeup_Color { get; set; }
    public ConcurrentDictionary<string, FeatureVector> FaceNoMakeup_Color { get; set; }
    public ConcurrentDictionary<string, FeatureVector> FaceMakeup_Shape { get; set; }
    public ConcurrentDictionary<string, FeatureVector> FaceNoMakeup_Shape { get; set; }
    public ConcurrentDictionary<string, FeatureVector> FaceMakeup_Texture { get; set; }
    public ConcurrentDictionary<string, FeatureVector> FaceNoMakeup_Texture { get; set; }
  }
}
