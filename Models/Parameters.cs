﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognition.Models
{
  public class Parameters
  {
    public int Cost { get; set; }
    public int Degree { get; set; }
    public double Gamma { get; set; }

    public Parameters(int Cost, int Degree, double Gamma)
    {
      this.Cost = Cost;
      this.Degree = Degree;
      this.Gamma = Gamma;
    }
  }
}
