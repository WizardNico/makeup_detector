﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BioLab.Biometrics.Face;
using BioLab.Classification.Supervised;
using BioLab.Common;
using System.IO;
using BioLab.ImageProcessing;
using BioLab.GUI.Forms;
using System.Threading.Tasks;
using FaceRecognition.Models;
using FaceRecognition.Utilities;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace FaceRecognition
{
   public partial class FormFaceDetection : Form
   {
      private ClassifierFaceDetector FaceDetector, MouthDetector, RightEyeDetector, LeftEyeDetector;
      private SvmClassifier FaceMakeupClassifier;
      private bool drawResult = false;
      private bool drawFace = false;
      private bool drawRoi = false;

      private RgbImage<byte> Mouth, LeftEye, RightEye, Face;

      public FormFaceDetection()
      {
         FilterFactory.GaborFilterBank = FilterFactory.GetGaborFilterBank(8, 5, false);
         InitializeComponent();
         Parallel.Invoke(() => InitMouthDetector(), () => InitEyeDetector());
      }

      private void ResetData()
      {
         Face = null;
         Mouth = null;
         LeftEye = null;
         RightEye = null;
         drawResult = false;
         drawFace = false;
         drawRoi = false;
         btn_makeup.Enabled = false;
      }

      private void UpdateParams(int classificator)
      {
         if (FaceDetector != null && classificator == 1)
         {
            if (textBoxMinWindowSize.Text != "")
            {
               FaceDetector.MinimumWindowSize = Convert.ToInt32(textBoxMinWindowSize.Text);
            }
            else
            {
               textBoxMinWindowSize.Text = FaceDetector.MinimumWindowSize.ToString();
            }
            if (textBoxMaxScaleFactor.Text != "")
            {
               FaceDetector.MaximumScaleFactor = Convert.ToDouble(textBoxMaxScaleFactor.Text);
            }
            else
            {
               textBoxMaxScaleFactor.Text = FaceDetector.MaximumScaleFactor.ToString();
            }
            if (textBoxScaleIncStep.Text != "")
            {
               FaceDetector.ScaleIncrementStep = Convert.ToDouble(textBoxScaleIncStep.Text);
            }
            else
            {
               textBoxScaleIncStep.Text = FaceDetector.ScaleIncrementStep.ToString();
            }
         }

         if (RightEyeDetector != null && classificator == 2)
         {
            if (textBoxMinWindowSize.Text != "")
            {
               RightEyeDetector.MinimumWindowSize = Convert.ToInt32(textBoxMinWindowSize.Text);
            }
            else
            {
               textBoxMinWindowSize.Text = RightEyeDetector.MinimumWindowSize.ToString();
            }
            if (textBoxMaxScaleFactor.Text != "")
            {
               RightEyeDetector.MaximumScaleFactor = Convert.ToDouble(textBoxMaxScaleFactor.Text);
            }
            else
            {
               textBoxMaxScaleFactor.Text = RightEyeDetector.MaximumScaleFactor.ToString();
            }
            if (textBoxScaleIncStep.Text != "")
            {
               RightEyeDetector.ScaleIncrementStep = Convert.ToDouble(textBoxScaleIncStep.Text);
            }
            else
            {
               textBoxScaleIncStep.Text = RightEyeDetector.ScaleIncrementStep.ToString();
            }
         }

         if (MouthDetector != null && classificator == 3)
         {
            if (textBoxMinWindowSize.Text != "")
            {
               MouthDetector.MinimumWindowSize = Convert.ToInt32(textBoxMinWindowSize.Text);
            }
            else
            {
               textBoxMinWindowSize.Text = MouthDetector.MinimumWindowSize.ToString();
            }
            if (textBoxMaxScaleFactor.Text != "")
            {
               MouthDetector.MaximumScaleFactor = Convert.ToDouble(textBoxMaxScaleFactor.Text);
            }
            else
            {
               textBoxMaxScaleFactor.Text = MouthDetector.MaximumScaleFactor.ToString();
            }
            if (textBoxScaleIncStep.Text != "")
            {
               MouthDetector.ScaleIncrementStep = Convert.ToDouble(textBoxScaleIncStep.Text);
            }
            else
            {
               textBoxScaleIncStep.Text = MouthDetector.ScaleIncrementStep.ToString();
            }
         }
      }

      private void buttonTraining_Click(object sender, EventArgs e)
      {
         Parallel.Invoke(() => InitFaceDetector(), () => InitMakeupDetector(6, 3, 0.001));

         UpdateParams(1);
         lbl_complete.Visible = true;
      }

      private void buttonPreview_Click(object sender, EventArgs e)
      {
         if (FaceDetector != null)
         {
            UpdateParams(1);
            var previewForm = new AlgorithmPreviewForm(FaceDetector);
            previewForm.ShowDialog();
            textBoxMinWindowSize.Text = FaceDetector.MinimumWindowSize.ToString();
            textBoxMaxScaleFactor.Text = FaceDetector.MaximumScaleFactor.ToString();
            textBoxScaleIncStep.Text = FaceDetector.ScaleIncrementStep.ToString();
         }
      }

      private void ButtonDetection_Click(object sender, EventArgs e)
      {
         lbl_Result.Visible = false;

         ResetData();

         if (FaceDetector != null)
         {
            drawFace = true;
            drawRoi = false;

            UpdateParams(1);
            FaceDetector.Detect(imageViewerInputImage.Image.ToByteImage());
            imageViewerFaceCandidates.Image = FaceDetector.InputImage;
            drawResult = true;

            if (FaceDetector.Result.Count != 0)
            {
               var rect = FaceDetector.Result.ElementAt(0).Rectangle;

               var img = ImageUtilities.Resize(imageViewerInputImage.Image.GetSubImage(rect.X, rect.Y, rect.Width, rect.Height).ToByteRgbImage(), 130, 150);

               var brightnessAdjustment = new BrightnessAdjustmentRGB
               {
                  InputImage = img,
                  BrightnessVaration = 10
               };
               img = brightnessAdjustment.Execute();

               Face = new RgbImage<byte>(
                  new HistogramAdjustment(img.RedChannel, true, false, 1).Execute(),
                  new HistogramAdjustment(img.GreenChannel, true, false, 1).Execute(),
                  new HistogramAdjustment(img.BlueChannel, true, false, 1).Execute()
                  );
               Face.SaveToFile(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + "\\Faces\\face.bmp");
            }
         }
      }

      private void fileListControlInputFiles_SelectedFileNameChanged(object sender, EventArgs e)
      {
         imageViewerInputImage.Image = ImageBase.LoadFromFile(fileListControlInputFiles.SelectedFileName).ToByteRgbImage();
         imageViewerFaceCandidates.Image = null;
         FaceDetector.InputImage = imageViewerInputImage.Image.ToByteImage();
         drawResult = false;
      }

      private void imageViewerFaceCandidates_Paint(object sender, PaintEventArgs e)
      {
         base.OnPaint(e);
         imageViewerFaceCandidates.AdjustGraphicsToWorldUnits(e.Graphics);
         if (FaceDetector != null && !drawRoi && drawFace && drawResult)
         {
            DrowFaceRectagnle(e);
         }

         if (LeftEyeDetector != null && RightEyeDetector != null && drawRoi && !drawFace && drawResult)
         {
            DrowEyesRectagnles(e);
            CheckToEnableMakeupDetection();
         }

         if (MouthDetector != null && drawRoi && !drawFace && drawResult)
         {
            DrowMouthRectagnle(e);
            CheckToEnableMakeupDetection();
         }
      }

      private void DrowFaceRectagnle(PaintEventArgs e)
      {
         Graphics gr = e.Graphics;
         if (FaceDetector.Result != null)
         {
            Pen penRes = new Pen(Color.Green, 1);
            foreach (FaceLocationCandidate candidate in FaceDetector.Result)
            {
               gr.DrawRectangle(penRes, candidate.Rectangle.ToRectangle());
               btn_eyes_roi.Enabled = true;
               btn_mouth_roi.Enabled = true;
            }
         }
      }

      private void DrowEyesRectagnles(PaintEventArgs e)
      {
         Graphics gr = e.Graphics;
         if (RightEyeDetector.Result != null)
         {
            Pen penRes = new Pen(Color.Blue, 1);
            foreach (FaceLocationCandidate candidate in RightEyeDetector.Result)
            {
               gr.DrawRectangle(penRes, candidate.Rectangle.ToRectangle());
            }
         }

         if (LeftEyeDetector.Result != null)
         {
            Pen penRes = new Pen(Color.Blue, 1);
            foreach (FaceLocationCandidate candidate in LeftEyeDetector.Result)
            {
               var r = new Rectangle((candidate.Rectangle.X + (imageViewerInputImage.Image.Width / 2)), candidate.Rectangle.Y, candidate.Rectangle.Width, candidate.Rectangle.Height);
               gr.DrawRectangle(penRes, r);
            }
         }
      }

      private void DrowMouthRectagnle(PaintEventArgs e)
      {
         if (MouthDetector.Result != null)
         {
            Graphics gr = e.Graphics;
            Pen penRes = new Pen(Color.Yellow, 1);
            foreach (FaceLocationCandidate candidate in MouthDetector.Result)
            {
               var r = new Rectangle(candidate.Rectangle.X, (candidate.Rectangle.Y + (imageViewerInputImage.Image.Height / 2)), candidate.Rectangle.Width, candidate.Rectangle.Height);
               gr.DrawRectangle(penRes, r);
            }
         }
      }

      private void CheckToEnableMakeupDetection()
      {
         if (LeftEyeDetector != null && RightEyeDetector != null && MouthDetector != null)
         {
            if ((LeftEyeDetector.Result != null && RightEyeDetector.Result != null && MouthDetector.Result != null))
            {
               btn_makeup.Enabled = true;
            }
         }
      }

      private void InitEyeDetector()
      {
         var featureExtractor = new MyHaarFaceFeatureExtractor();
         FeatureVectorSet trainingSet = new FeatureVectorSet();

         trainingSet = GetFeatureSet(Repository.BASE_PATH + Repository.TRAINING_SET_EYES_PATH);

         if (trainingSet == null)
         {
            trainingSet = new FeatureVectorSet();
            FeatureVector f;
            Image<byte> image;

            String[] files_left_eye = Directory.GetFiles(Repository.BASE_PATH + Repository.TRAIN_LEFT_EYES_PATH, "*.jpg");
            String[] files_right_eye = Directory.GetFiles(Repository.BASE_PATH + Repository.TRAIN_RIGHT_EYES_PATH, "*.jpg");
            String[] files_non_eye = Directory.GetFiles(Repository.BASE_PATH + Repository.TRAIN_NO_EYES_PATH, "*.bmp");

            //Left eyes open & closed
            foreach (var imagePath in files_left_eye)
            {
               image = ImageBase.LoadFromFile(imagePath).ToByteImage();
               f = featureExtractor.ExtractFeatures(image);
               f.Class = 1;
               trainingSet.Add(f, false);
            }

            //Right eyes open & closed
            foreach (var imagePath in files_right_eye)
            {
               image = ImageBase.LoadFromFile(imagePath).ToByteImage();
               f = featureExtractor.ExtractFeatures(image);
               f.Class = 1;
               trainingSet.Add(f, false);
            }

            //Non_eye
            foreach (var imagePath in files_non_eye)
            {
               image = ImageBase.LoadFromFile(imagePath).ToByteImage();
               if (image.Width != 24 || image.Height != 24)
                  image = ImageUtilities.Resize(image, 24, 24);

               f = featureExtractor.ExtractFeatures(image);
               f.Class = 0;
               trainingSet.Add(f, false);
            }

            trainingSet.SaveToTextFile(Repository.BASE_PATH + Repository.TRAINING_SET_EYES_PATH);
         }

         SvmClassifierBuilder svmBuilder = new SvmClassifierBuilder(trainingSet)
         {
            SvmType = SvmTypes.C_SVC,
            Cost = 50,
            KernelType = KernelType.Linear,
            Degree = 4,
            Gamma = 0.1
         };

         var classifier = svmBuilder.Train();
         RightEyeDetector = new ClassifierFaceDetector(featureExtractor, classifier);
      }

      private void InitMouthDetector()
      {
         FeatureVectorSet trainingSet = new FeatureVectorSet();
         var featureExtractor = new MyHaarFaceFeatureExtractor();

         trainingSet = GetFeatureSet(Repository.BASE_PATH + Repository.TRAINING_SET_MOUTH_PATH);

         if (trainingSet == null)
         {
            trainingSet = new FeatureVectorSet();
            FeatureVector f;
            Image<byte> image;

            String[] files_mouths = Directory.GetFiles(Repository.BASE_PATH + Repository.TRAIN_MOUTH_PATH, "*.bmp");
            String[] files_non_mouths = Directory.GetFiles(Repository.BASE_PATH + Repository.TRAIN_NO_MOUTH_PATH, "*.bmp");

            //Mouth
            foreach (var imagePath in files_mouths)
            {
               image = ImageBase.LoadFromFile(imagePath).ToByteImage();
               if (image.Width != 20 || image.Height != 20)
                  image = ImageUtilities.Resize(image, 20, 20);

               f = featureExtractor.ExtractFeatures(image);
               f.Class = 1;
               trainingSet.Add(f, false);
            }

            //Non-Mouth
            foreach (var imagePath in files_non_mouths)
            {
               image = ImageBase.LoadFromFile(imagePath).ToByteImage();
               if (image.Width != 20 || image.Height != 20)
                  image = ImageUtilities.Resize(image, 20, 20);

               f = featureExtractor.ExtractFeatures(image);
               f.Class = 0;
               trainingSet.Add(f, false);
            }

            trainingSet.SaveToTextFile(Repository.BASE_PATH + Repository.TRAINING_SET_MOUTH_PATH);
         }

         SvmClassifierBuilder svmBuilder = new SvmClassifierBuilder(trainingSet)
         {
            SvmType = SvmTypes.C_SVC,
            Cost = 10,
            KernelType = KernelType.Polynomial,
            Degree = 2,
            Gamma = 0.1
         };

         var classifier = svmBuilder.Train();
         MouthDetector = new ClassifierFaceDetector(featureExtractor, classifier);
      }

      private void InitFaceDetector()
      {
         FeatureVectorSet trainingSet = new FeatureVectorSet();
         var featureExtractor = new MyHaarFaceFeatureExtractor();

         trainingSet = GetFeatureSet(Repository.BASE_PATH + Repository.TRAINING_SET_FACE_PATH);

         if (trainingSet == null)
         {
            trainingSet = new FeatureVectorSet();
            FeatureVector f;
            Image<byte> image;

            string[] faceImages = File.ReadAllLines(Repository.BASE_PATH + Repository.TRAIN_FACE_PATH);
            string[] nonFaceImages = File.ReadAllLines(Repository.BASE_PATH + Repository.TRAIN_NO_FACE_PATH);

            // Face
            Parallel.ForEach(faceImages, (currentFile) =>
            {
               image = ImageBase.LoadFromFile(Repository.BASE_PATH + "\\DBFaceNonFace\\Training_Face\\" + currentFile).ToByteImage();
               if (image.Width != 19 || image.Height != 19)
                  image = ImageUtilities.Resize(image, 19, 19);

               f = featureExtractor.ExtractFeatures(image);
               f.Class = 1;
               trainingSet.Add(f, false);
            });

            // Non face
            Parallel.ForEach(nonFaceImages, (currentFile) =>
            {
               image = ImageBase.LoadFromFile(Repository.BASE_PATH + "\\DBFaceNonFace\\Training_NonFace\\" + currentFile).ToByteImage();
               if (image.Width != 19 || image.Height != 19)
                  image = ImageUtilities.Resize(image, 19, 19);

               f = featureExtractor.ExtractFeatures(image);
               f.Class = 0;
               trainingSet.Add(f, false);
            });

            trainingSet.SaveToTextFile(Repository.BASE_PATH + Repository.TRAINING_SET_FACE_PATH);
         }

         SvmClassifierBuilder svmBuilder = new SvmClassifierBuilder(trainingSet)
         {
            SvmType = SvmTypes.C_SVC,
            Cost = 10,
            KernelType = KernelType.Polynomial,
            Degree = 2,
            Gamma = 0.1
         };

         var classifier = svmBuilder.Train();
         FaceDetector = new ClassifierFaceDetector(featureExtractor, classifier);
      }

      private void FormFaceDetection_Load(object sender, EventArgs e)
      {
      }

      private void btn_eye_roi_Click(object sender, EventArgs e)
      {
         RgbImage<byte> face = ImageBase.LoadFromFile(Repository.BASE_PATH + "\\Faces\\face.bmp").ToByteRgbImage();

         imageViewerInputImage.Image = face;
         imageViewerFaceCandidates.Image = face;

         UpdateParams(2);
         DetectEyesAndDrow(face);

         Face = ImageBase.LoadFromFile(Repository.BASE_PATH + "\\Faces\\face.bmp").ToByteRgbImage();
      }

      private void btn_mouth_roi_Click(object sender, EventArgs e)
      {
         RgbImage<byte> face = ImageBase.LoadFromFile(Repository.BASE_PATH + "\\Faces\\face.bmp").ToByteRgbImage();

         imageViewerInputImage.Image = face;
         imageViewerFaceCandidates.Image = face;

         UpdateParams(3);
         DetectMouthAndDrow(face);

         Face = ImageBase.LoadFromFile(Repository.BASE_PATH + "\\Faces\\face.bmp").ToByteRgbImage();
      }

      private RgbImage<byte> GetMouth(RgbImage<byte> face)
      {
         RgbImage<byte> bottom = face.GetSubImage(0, face.Center.Y, face.Width, face.Height / 2).ToByteRgbImage();
         RgbImage<byte> res = null;

         lock (MouthDetector)
         {
            MouthDetector.MinimumWindowSize = 55;
            MouthDetector.MaximumScaleFactor = 2;
            MouthDetector.ScaleIncrementStep = 0.5;

            MouthDetector.Detect(bottom.ToByteImage());
            res = ImageUtilities.Resize(bottom.GetSubImage(MouthDetector.Result.ElementAt(0).Rectangle.X, MouthDetector.Result.ElementAt(0).Rectangle.Y, MouthDetector.Result.ElementAt(0).Rectangle.Width, MouthDetector.Result.ElementAt(0).Rectangle.Height).ToByteRgbImage(), 62, 52);
         }

         return res;
      }

      private List<RgbImage<byte>> GetEyes(RgbImage<byte> face)
      {
         List<RgbImage<byte>> Eyes = new List<RgbImage<byte>>();
         RgbImage<byte> top_right = face.GetSubImage(0, 0, face.Center.X, face.Height / 2).ToByteRgbImage();
         RgbImage<byte> top_left = face.GetSubImage(face.Width / 2, 0, face.Width / 2, face.Height / 2).ToByteRgbImage();

         LeftEyeDetector = RightEyeDetector;

         lock (RightEyeDetector)
         {
            RightEyeDetector.MinimumWindowSize = 55;
            RightEyeDetector.MaximumScaleFactor = 1;
            RightEyeDetector.ScaleIncrementStep = 0.5;

            lock (LeftEyeDetector)
            {
               LeftEyeDetector.MinimumWindowSize = 55;
               LeftEyeDetector.MaximumScaleFactor = 1;
               LeftEyeDetector.ScaleIncrementStep = 0.5;

               Parallel.Invoke(() => RightEyeDetector.Detect(top_right.ToByteImage()), () => LeftEyeDetector.Detect(top_left.ToByteImage()));

               switch (RightEyeDetector.Result.Count)
               {
                  case 0:
                     while (RightEyeDetector.Result.Count == 0)
                     {
                        RightEyeDetector.MaximumScaleFactor += 1;
                        RightEyeDetector.MinimumWindowSize -= 5;
                        RightEyeDetector.Detect(top_right.ToByteImage());
                     }
                     break;

                  default:
                     {
                        Eyes.Add(ImageUtilities.Resize(top_right.GetSubImage(RightEyeDetector.Result.ElementAt(0).Rectangle.X, RightEyeDetector.Result.ElementAt(0).Rectangle.Y, RightEyeDetector.Result.ElementAt(0).Rectangle.Width, RightEyeDetector.Result.ElementAt(0).Rectangle.Height).ToByteRgbImage(), 52, 52));
                        break;
                     }
               }

               switch (LeftEyeDetector.Result.Count)
               {
                  case 0:
                     while (LeftEyeDetector.Result.Count == 0)
                     {
                        LeftEyeDetector.MaximumScaleFactor += 1;
                        LeftEyeDetector.MinimumWindowSize -= 5;
                        LeftEyeDetector.Detect(top_left.ToByteImage());
                     }
                     break;

                  default:
                     {
                        Eyes.Add(ImageUtilities.Resize(top_left.GetSubImage(LeftEyeDetector.Result.ElementAt(0).Rectangle.X, LeftEyeDetector.Result.ElementAt(0).Rectangle.Y, LeftEyeDetector.Result.ElementAt(0).Rectangle.Width, LeftEyeDetector.Result.ElementAt(0).Rectangle.Height).ToByteRgbImage(), 52, 52));
                        break;
                     }
               }
            }
         }

         return Eyes;
      }

      private void DetectEyesAndDrow(RgbImage<byte> face)
      {
         drawFace = false;
         drawRoi = true;

         RgbImage<byte> top_right = face.GetSubImage(0, 0, face.Center.X, face.Height / 2).ToByteRgbImage();
         RgbImage<byte> top_left = face.GetSubImage(face.Width / 2, 0, face.Width / 2, face.Height / 2).ToByteRgbImage();

         if (RightEyeDetector == null)
         {
            InitEyeDetector();
            InitParams(2, top_right.Width, top_right.Height);
         }

         LeftEyeDetector = RightEyeDetector;

         Parallel.Invoke(() => RightEyeDetector.Detect(top_right.ToByteImage()), () => LeftEyeDetector.Detect(top_left.ToByteImage()));

         if (RightEyeDetector.Result.Count != 0)
         {
            var right_eye = top_right.GetSubImage(RightEyeDetector.Result.ElementAt(0).Rectangle.X, RightEyeDetector.Result.ElementAt(0).Rectangle.Y, RightEyeDetector.Result.ElementAt(0).Rectangle.Width, RightEyeDetector.Result.ElementAt(0).Rectangle.Height).ToByteRgbImage();
            RightEye = ImageUtilities.Resize(right_eye, 52, 52);
            RightEye.SaveToFile(Repository.BASE_PATH + "\\Faces\\right_eye.bmp");
         }

         if (LeftEyeDetector.Result.Count != 0)
         {
            var left_eye = top_left.GetSubImage(LeftEyeDetector.Result.ElementAt(0).Rectangle.X, LeftEyeDetector.Result.ElementAt(0).Rectangle.Y, LeftEyeDetector.Result.ElementAt(0).Rectangle.Width, LeftEyeDetector.Result.ElementAt(0).Rectangle.Height).ToByteRgbImage();
            LeftEye = ImageUtilities.Resize(left_eye, 52, 52);
            LeftEye.SaveToFile(Repository.BASE_PATH + "\\Faces\\left_eye.bmp");
         }

         drawResult = true;
      }

      private void DetectMouthAndDrow(RgbImage<byte> face)
      {
         drawFace = false;
         drawRoi = true;

         RgbImage<byte> bottom = face.GetSubImage(0, face.Center.Y, face.Width, face.Height / 2).ToByteRgbImage();

         if (MouthDetector == null)
         {
            InitMouthDetector();
            InitParams(3, bottom.Width, bottom.Height);
         }

         MouthDetector.Detect(bottom.ToByteImage());

         if (MouthDetector.Result.Count != 0)
         {
            Mouth = ImageUtilities.Resize(bottom.GetSubImage(MouthDetector.Result.ElementAt(0).Rectangle.X, MouthDetector.Result.ElementAt(0).Rectangle.Y, MouthDetector.Result.ElementAt(0).Rectangle.Width, MouthDetector.Result.ElementAt(0).Rectangle.Height).ToByteRgbImage(), 62, 52);
            Mouth.SaveToFile(Repository.BASE_PATH + "\\Faces\\mouth.bmp");
         }

         drawResult = true;
      }

      private void InitParams(int cl, int w, int h)
      {
         if (cl == 2)
         {
            textBoxMinWindowSize.Text = (55).ToString();
            RightEyeDetector.MinimumWindowSize = Convert.ToInt32(textBoxMinWindowSize.Text);

            textBoxMaxScaleFactor.Text = 1.ToString();
            RightEyeDetector.MaximumScaleFactor = Convert.ToDouble(textBoxMaxScaleFactor.Text);

            textBoxScaleIncStep.Text = (0.5).ToString();
            RightEyeDetector.ScaleIncrementStep = Convert.ToDouble(textBoxScaleIncStep.Text);
         }

         if (cl == 3)
         {
            textBoxMinWindowSize.Text = ((w / 2) + 5).ToString();
            MouthDetector.MinimumWindowSize = Convert.ToInt32(textBoxMinWindowSize.Text);

            textBoxMaxScaleFactor.Text = 2.ToString();
            MouthDetector.MaximumScaleFactor = Convert.ToDouble(textBoxMaxScaleFactor.Text);

            textBoxScaleIncStep.Text = (0.5).ToString();
            MouthDetector.ScaleIncrementStep = Convert.ToDouble(textBoxScaleIncStep.Text);
         }
      }

      private void btn_makeup_Click(object sender, EventArgs e)
      {
         FeatureVector featureColorFace = null;
         FeatureVector featureColorRoi = null;
         FeatureVector featureShapeFace = null;
         FeatureVector featureTextureFace = null;

         Stopwatch stopWatch = new Stopwatch();
         stopWatch.Start();

         Parallel.Invoke(
            () => featureShapeFace = GetFaceShapeFeature(Face),
            () => featureColorRoi = GetROIFeatureColor(Mouth, RightEye, LeftEye),
            () => featureColorFace = GetFaceFeatureColor(Face),
            () => featureTextureFace = GetFaceTextureFeature(Face)
            );

         if (FaceMakeupClassifier.Classify(FeatureVectorMerger.Merge(featureColorRoi, featureColorFace, featureShapeFace, featureTextureFace)) == 1)
         {
            lbl_Result.Text = "This face has makup!";
            lbl_Result.ForeColor = Color.Lime;
            lbl_Result.Visible = true;
         }
         else
         {
            lbl_Result.Text = "This face has no makup!";
            lbl_Result.ForeColor = Color.Red;
            lbl_Result.Visible = true;
         }

         stopWatch.Stop();
         Logger.Write(stopWatch.Elapsed);
      }

      private void btn_test_Click(object sender, EventArgs e)
      {
         //List<Parameters> parameters = GridSearch(KernelType.RadialBasisFunction);
         Stopwatch stopWatch = new Stopwatch();
         stopWatch.Start();

         var dataModel = new GridSearchModel()
         {
            Roi_Makeup = GetROIFeatureColor(Repository.BASE_PATH + Repository.TEST_MAKEUP_PATH, "jpg"),
            Roi_NoMakeup = GetROIFeatureColor(Repository.BASE_PATH + Repository.TEST_NO_MAKEUP_PATH, "jpg"),
            FaceMakeup_Color = GetFaceFeatureColor(Repository.BASE_PATH + Repository.TEST_MAKEUP_PATH, "jpg"),
            FaceNoMakeup_Color = GetFaceFeatureColor(Repository.BASE_PATH + Repository.TEST_NO_MAKEUP_PATH, "jpg"),
            FaceMakeup_Shape = GetFaceShapeFeature(Repository.BASE_PATH + Repository.TEST_MAKEUP_PATH, "jpg"),
            FaceNoMakeup_Shape = GetFaceShapeFeature(Repository.BASE_PATH + Repository.TEST_NO_MAKEUP_PATH, "jpg"),
            FaceMakeup_Texture = GetFaceTextureFeature(Repository.BASE_PATH + Repository.TEST_MAKEUP_PATH, "jpg"),
            FaceNoMakeup_Texture = GetFaceTextureFeature(Repository.BASE_PATH + Repository.TEST_NO_MAKEUP_PATH, "jpg")
         };

         var parameters = new List<Parameters>(){
            //new Parameters(2,3,0.001),
            //new Parameters(4,3,0.001),
            new Parameters(6,3,0.001),
            //new Parameters(4,3,0.0012),
            //new Parameters(2,3,0.0022),
         };

         foreach (var p in parameters)
         {
            TestDetector(p.Cost, p.Degree, p.Gamma, dataModel);
         }

         stopWatch.Stop();
         Logger.Write(stopWatch.Elapsed);

         lbl_test.Visible = true;
      }

      private FeatureVector GetFaceFeatureColor(RgbImage<byte> Image)
      {
         return new ColorFeatureExtractor(new HsvImage<double>(Image), ImageType.Face).GetFeatureVector();
      }

      private FeatureVector GetROIFeatureColor(RgbImage<byte> Mouth, RgbImage<byte> RightEye, RgbImage<byte> LeftEye)
      {
         FeatureVector fc_rightEye = null;
         FeatureVector fc_leftEye = null;
         FeatureVector fc_mouth = null;
         Parallel.Invoke(
                         () => fc_rightEye = new ColorFeatureExtractor(new HsvImage<double>(RightEye), ImageType.ROI).GetFeatureVector(),
                         () => fc_leftEye = new ColorFeatureExtractor(new HsvImage<double>(LeftEye), ImageType.ROI).GetFeatureVector(),
                         () => fc_mouth = new ColorFeatureExtractor(new HsvImage<double>(Mouth), ImageType.ROI).GetFeatureVector()
                         );

         return FeatureVectorMerger.Merge(fc_rightEye, fc_leftEye, fc_mouth);
      }

      private FeatureVector GetFaceShapeFeature(RgbImage<byte> Image)
      {
         FeatureVector sd_face = null;
         Image<double>[] gaborFaces = null;
         Image<double>[] spectrumImages = null;
         Parallel.Invoke(
                         () => sd_face = new ShapeFeatureExtractor(Image).GetEOH(),
                         () => gaborFaces = new ShapeFeatureExtractor(Image).GetGaborImages(FilterFactory.GaborFilterBank, false),
                         () => spectrumImages = new ShapeFeatureExtractor(Image).GetGISTImages(FilterFactory.GaborFilterBank.Take(32).ToArray(), false)
                        );

         var gaborFeatures = gaborFaces
                            .AsParallel()
                            .AsOrdered()
                            .WithDegreeOfParallelism(Repository.MAX_THREAD_NUMBER / 2)
                            .Select(img => ExtractFeatures(img, ImageType.Gabor))
                            .SelectMany(i => i)
                            .ToArray();

         var gistFeatures = spectrumImages
                           .AsParallel()
                           .AsOrdered()
                           .WithDegreeOfParallelism(Repository.MAX_THREAD_NUMBER / 2)
                           .Select(img => ExtractFeatures(img, ImageType.GIST))
                           .SelectMany(i => i)
                           .ToArray();

         return new FeatureVector(sd_face.Concat(gaborFeatures.Concat(gistFeatures)).ToArray());
      }

      private FeatureVector GetFaceTextureFeature(RgbImage<byte> Image)
      {
         return new TextureFeatureExtractor(Image.ToByteImage()).GetHistogramLBP();
      }

      private ConcurrentDictionary<string, FeatureVector> GetROIFeatureColor(string Path, string Type)
      {
         String[] files_makeup = Directory.GetFiles(Path, "*." + Type);

         //Calcolo le Feaure color degli occhi e bocca
         ConcurrentDictionary<string, FeatureVector> fc_roi = new ConcurrentDictionary<string, FeatureVector>();
         Parallel.ForEach(files_makeup, new ParallelOptions { MaxDegreeOfParallelism = Repository.MAX_THREAD_NUMBER }, (currentFile) =>
         {
            var Image = ImageBase.LoadFromFile(currentFile).ToByteRgbImage();
            var eyes = GetEyes(Image);
            var mouth = GetMouth(Image);

            FeatureVector fc_rightEye = null;
            FeatureVector fc_leftEye = null;
            FeatureVector fc_mouth = null;

            Parallel.Invoke(
                            () => fc_rightEye = new ColorFeatureExtractor(new HsvImage<double>(eyes.ElementAt(0)), ImageType.ROI).GetFeatureVector(),
                            () => fc_leftEye = new ColorFeatureExtractor(new HsvImage<double>(eyes.ElementAt(1)), ImageType.ROI).GetFeatureVector(),
                            () => fc_mouth = new ColorFeatureExtractor(new HsvImage<double>(mouth), ImageType.ROI).GetFeatureVector()
                           );

            fc_roi.TryAdd(currentFile, new FeatureVector(fc_rightEye.Concat(fc_leftEye.Concat(fc_mouth)).ToArray()));
         });

         return fc_roi;
      }

      private ConcurrentDictionary<string, FeatureVector> GetFaceFeatureColor(string Path, string Type)
      {
         String[] files_makeup = Directory.GetFiles(Path, "*." + Type);

         //Calcolo le Feaure color del volto
         ConcurrentDictionary<string, FeatureVector> fc_faces = new ConcurrentDictionary<string, FeatureVector>();
         Parallel.ForEach(files_makeup, new ParallelOptions { MaxDegreeOfParallelism = Repository.MAX_THREAD_NUMBER }, (currentFile) =>
          {
             var Image = ImageBase.LoadFromFile(currentFile).ToByteRgbImage();

             fc_faces.TryAdd(currentFile, new ColorFeatureExtractor(new HsvImage<double>(Image), ImageType.Face).GetFeatureVector());
          });

         return fc_faces;
      }

      private ConcurrentDictionary<string, FeatureVector> GetFaceShapeFeature(string Path, string Type)
      {
         String[] files_makeup = Directory.GetFiles(Path, "*." + Type);

         //Calcolo le Feaure color del volto
         ConcurrentDictionary<string, FeatureVector> sp_faces = new ConcurrentDictionary<string, FeatureVector>();
         Parallel.ForEach(files_makeup, new ParallelOptions { MaxDegreeOfParallelism = Repository.MAX_THREAD_NUMBER }, (currentFile) =>
         {
            var Image = ImageBase.LoadFromFile(currentFile).ToByteRgbImage();

            FeatureVector sd_face = null;
            Image<double>[] gaborFaces = null;
            Image<double>[] spectrumImages = null;
            Parallel.Invoke(
                            () => sd_face = new ShapeFeatureExtractor(Image).GetEOH(),
                            () => gaborFaces = new ShapeFeatureExtractor(Image).GetGaborImages(FilterFactory.GaborFilterBank, false),
                            () => spectrumImages = new ShapeFeatureExtractor(Image).GetGISTImages(FilterFactory.GaborFilterBank.Take(32).ToArray(), false)
                           );

            var gaborFeatures = gaborFaces
                               .AsParallel()
                               .AsOrdered()
                               .WithDegreeOfParallelism((Repository.MAX_THREAD_NUMBER - 1) / 2)
                               .Select(img => ExtractFeatures(img, ImageType.Gabor))
                               .SelectMany(i => i)
                               .ToArray();

            var gistFeatures = spectrumImages
                              .AsParallel()
                              .AsOrdered()
                              .WithDegreeOfParallelism((Repository.MAX_THREAD_NUMBER - 1) / 2)
                              .Select(img => ExtractFeatures(img, ImageType.GIST))
                              .SelectMany(i => i)
                              .ToArray();

            sp_faces.TryAdd(currentFile, new FeatureVector(sd_face.Concat(gaborFeatures.Concat(gistFeatures)).ToArray()));
         });

         return sp_faces;
      }

      private double[] ExtractFeatures(Image<double> Image, ImageType Type)
      {
         if (Type == ImageType.Gabor)
            return new ColorFeatureExtractor(Image, ImageType.Gabor).GetFeatureVector().ToArray();
         else
            return new ColorFeatureExtractor(Image, ImageType.GIST).GetFeatureVector().ToArray();
      }

      private ConcurrentDictionary<string, FeatureVector> GetFaceTextureFeature(string Path, string Type)
      {
         String[] files_makeup = Directory.GetFiles(Path, "*." + Type);

         //Calcolo le Feaure color del volto
         ConcurrentDictionary<string, FeatureVector> txt_faces = new ConcurrentDictionary<string, FeatureVector>();
         Parallel.ForEach(files_makeup, new ParallelOptions { MaxDegreeOfParallelism = Repository.MAX_THREAD_NUMBER }, (currentFile) =>
         {
            var Image = ImageBase.LoadFromFile(currentFile).ToByteRgbImage();
            txt_faces.TryAdd(currentFile, new TextureFeatureExtractor(Image.ToByteImage()).GetHistogramLBP());
         });

         return txt_faces;
      }

      private void TestDetector(int Cost, int Degree, double Gamma, GridSearchModel dataModel)
      {
         var false_negative = 0;
         var false_positive = 0;

         String[] files_makeup = Directory.GetFiles(Repository.BASE_PATH + Repository.TEST_MAKEUP_PATH, "*.jpg");
         String[] files_non_makeup = Directory.GetFiles(Repository.BASE_PATH + Repository.TEST_NO_MAKEUP_PATH, "*.jpg");

         InitMakeupDetector(Cost, Degree, Gamma);

         foreach (var currentFile in files_makeup)
         {
            if (FaceMakeupClassifier.Classify(FeatureVectorMerger.Merge(dataModel.Roi_Makeup[currentFile], dataModel.FaceMakeup_Color[currentFile], dataModel.FaceMakeup_Shape[currentFile], dataModel.FaceMakeup_Texture[currentFile])) == 0)
               false_negative++;
         }

         foreach (var currentFile in files_non_makeup)
         {
            if (FaceMakeupClassifier.Classify(FeatureVectorMerger.Merge(dataModel.Roi_NoMakeup[currentFile], dataModel.FaceNoMakeup_Color[currentFile], dataModel.FaceNoMakeup_Shape[currentFile], dataModel.FaceNoMakeup_Texture[currentFile])) == 1)
               false_positive++;
         }

         Logger.Write(false_positive, false_negative, Cost, Gamma, Degree);
      }

      private List<Parameters> GridSearch(KernelType Kernel)
      {
         List<Parameters> result = new List<Parameters>();
         IEnumerable<int> costs = Enumerable.Range(1, 90).Select(x => x * 2);
         IEnumerable<int> degrees = Enumerable.Range(1, 10);

         double[] gammas = { 0.0010, 0.0012, 0.0014, 0.0016, 0.0018, 0.0020, 0.0022, 0.0024, 0.0026, 0.0028, 0.0030, 0.0032, 0.0034, 0.0036, 0.0038, 0.0040, 0.0042, 0.0044, 0.0046, 0.0048, 0.0050, 0.0052, 0.0054, 0.0056, 0.0058, 0.0060, 0.0062, 0.0064, 0.0066, 0.0068, 0.0070, 0.0072, 0.0074, 0.0076, 0.0078, 0.0080 };

         int best_error = int.MaxValue;
         int best_cost = int.MaxValue;
         int best_degree = int.MaxValue;
         double best_gamma = int.MaxValue;

         var dataModel = new GridSearchModel()
         {
            Roi_Makeup = GetROIFeatureColor(Repository.BASE_PATH + Repository.VALIDATION_MAKEUP_PATH, "jpg"),
            Roi_NoMakeup = GetROIFeatureColor(Repository.BASE_PATH + Repository.VALIDATION_NO_MAKEUP_PATH, "jpg"),
            FaceMakeup_Color = GetFaceFeatureColor(Repository.BASE_PATH + Repository.VALIDATION_MAKEUP_PATH, "jpg"),
            FaceNoMakeup_Color = GetFaceFeatureColor(Repository.BASE_PATH + Repository.VALIDATION_NO_MAKEUP_PATH, "jpg"),
            FaceMakeup_Shape = GetFaceShapeFeature(Repository.BASE_PATH + Repository.VALIDATION_MAKEUP_PATH, "jpg"),
            FaceNoMakeup_Shape = GetFaceShapeFeature(Repository.BASE_PATH + Repository.VALIDATION_NO_MAKEUP_PATH, "jpg"),
            FaceMakeup_Texture = GetFaceTextureFeature(Repository.BASE_PATH + Repository.VALIDATION_MAKEUP_PATH, "jpg"),
            FaceNoMakeup_Texture = GetFaceTextureFeature(Repository.BASE_PATH + Repository.VALIDATION_NO_MAKEUP_PATH, "jpg")
         };

         switch (Kernel)
         {
            case KernelType.RadialBasisFunction:
               foreach (var gamma in gammas)
               {
                  foreach (var cost in costs)
                  {
                     var newError = GetError(cost, 3, gamma, dataModel);

                     if (newError < best_error)
                     {
                        best_cost = cost;
                        best_error = newError;
                        best_gamma = gamma;
                        result.Add(new Parameters(best_cost, 3, best_gamma));
                     }
                     else if (newError == best_error)
                     {
                        result.Add(new Parameters(cost, 3, gamma));
                     }

                     Logger.Write(Kernel, best_error, best_cost, best_gamma, 0);
                  }
               }
               break;

            case KernelType.Polynomial:
               foreach (var gamma in gammas)
               {
                  foreach (var degree in degrees)
                  {
                     foreach (var cost in costs)
                     {
                        var newError = GetError(cost, degree, gamma, dataModel);

                        if (newError < best_error)
                        {
                           best_cost = cost;
                           best_error = newError;
                           best_gamma = gamma;
                           best_degree = degree;
                        }
                        else if (newError == best_error)
                        {
                           result.Add(new Parameters(cost, degree, gamma));
                        }

                        Logger.Write(Kernel, best_error, best_cost, best_gamma, best_degree);
                     }
                  }
               }
               break;

            case KernelType.Linear:
               foreach (var cost in costs)
               {
                  var newError = GetError(cost, 0, 0, dataModel);

                  if (newError < best_error)
                  {
                     best_cost = cost;
                     best_error = newError;
                  }
                  else if (newError == best_error)
                  {
                     result.Add(new Parameters(cost, 0, 0));
                  }

                  Logger.Write(Kernel, best_error, best_cost, 0, 0);
               }
               break;
         }

         return result;
      }

      private int GetError(int Cost, int Degree, double Gamma, GridSearchModel Model)
      {
         InitMakeupDetector(Cost, Degree, Gamma);

         var false_negative = 0;
         var false_positive = 0;

         //Makeup
         foreach (var currentFile in Directory.GetFiles(Repository.BASE_PATH + Repository.VALIDATION_MAKEUP_PATH, "*.jpg"))
         {
            var c1 = FaceMakeupClassifier.Classify(FeatureVectorMerger.Merge(Model.Roi_Makeup[currentFile], Model.FaceMakeup_Color[currentFile], Model.FaceMakeup_Shape[currentFile], Model.FaceMakeup_Texture[currentFile]));
            if (c1 == 0)
               false_negative++;
         }

         //Non Makeup
         foreach (var currentFile in Directory.GetFiles(Repository.BASE_PATH + Repository.VALIDATION_NO_MAKEUP_PATH, "*.jpg"))
         {
            var c1 = FaceMakeupClassifier.Classify(FeatureVectorMerger.Merge(Model.Roi_NoMakeup[currentFile], Model.FaceNoMakeup_Color[currentFile], Model.FaceNoMakeup_Shape[currentFile], Model.FaceNoMakeup_Texture[currentFile]));
            if (c1 == 1)
               false_positive++;
         }

         return false_negative + false_positive;
      }

      private void InitMakeupDetector(int Cost, int Degree, double Gamma)
      {
         FeatureVectorSet trainingSet = GetFeatureSet(Repository.BASE_PATH + Repository.TRAINING_SET_MAKEUP_PATH);

         if (trainingSet == null)
         {
            trainingSet = new FeatureVectorSet();

            String[] files_makeup = Directory.GetFiles(Repository.BASE_PATH + Repository.TRAIN_MAKEUP_PATH, "*.jpg");
            String[] files_non_makeup = Directory.GetFiles(Repository.BASE_PATH + Repository.TRAIN_NO_MAKEUP_PATH, "*.jpg");

            Parallel.Invoke(() => InitMouthDetector(), () => InitEyeDetector());

            var dataModel = new GridSearchModel()
            {
               Roi_Makeup = GetROIFeatureColor(Repository.BASE_PATH + Repository.TRAIN_MAKEUP_PATH, "jpg"),
               Roi_NoMakeup = GetROIFeatureColor(Repository.BASE_PATH + Repository.TRAIN_NO_MAKEUP_PATH, "jpg"),
               FaceMakeup_Color = GetFaceFeatureColor(Repository.BASE_PATH + Repository.TRAIN_MAKEUP_PATH, "jpg"),
               FaceNoMakeup_Color = GetFaceFeatureColor(Repository.BASE_PATH + Repository.TRAIN_NO_MAKEUP_PATH, "jpg"),
               FaceMakeup_Shape = GetFaceShapeFeature(Repository.BASE_PATH + Repository.TRAIN_MAKEUP_PATH, "jpg"),
               FaceNoMakeup_Shape = GetFaceShapeFeature(Repository.BASE_PATH + Repository.TRAIN_NO_MAKEUP_PATH, "jpg"),
               FaceMakeup_Texture = GetFaceTextureFeature(Repository.BASE_PATH + Repository.TRAIN_MAKEUP_PATH, "jpg"),
               FaceNoMakeup_Texture = GetFaceTextureFeature(Repository.BASE_PATH + Repository.TRAIN_NO_MAKEUP_PATH, "jpg")
            };

            foreach (var currentFile in files_makeup)
            {
               var f = FeatureVectorMerger.Merge(dataModel.Roi_Makeup[currentFile], dataModel.FaceMakeup_Color[currentFile], dataModel.FaceMakeup_Shape[currentFile], dataModel.FaceMakeup_Texture[currentFile]);
               f.Class = 1;
               trainingSet.Add(f, false);
            }

            foreach (var currentFile in files_non_makeup)
            {
               var f = FeatureVectorMerger.Merge(dataModel.Roi_NoMakeup[currentFile], dataModel.FaceNoMakeup_Color[currentFile], dataModel.FaceNoMakeup_Shape[currentFile], dataModel.FaceNoMakeup_Texture[currentFile]);
               f.Class = 0;
               trainingSet.Add(f, false);
            }

            trainingSet.SaveToTextFile(Repository.BASE_PATH + Repository.TRAINING_SET_MAKEUP_PATH);
         }

         SvmClassifierBuilder svmBuilder = new SvmClassifierBuilder(trainingSet)
         {
            SvmType = SvmTypes.C_SVC,
            Cost = Cost,
            KernelType = KernelType.RadialBasisFunction,
            Degree = Degree,
            Gamma = Gamma
         };

         FaceMakeupClassifier = svmBuilder.Train();
      }

      private FeatureVectorSet GetFeatureSet(string Path)
      {
         FeatureVectorSet trainingSet = new FeatureVectorSet();
         try
         {
            trainingSet = FeatureVectorSet.LoadFromTextFile(Path);
         }
         catch (FileNotFoundException exc)
         {
            trainingSet = null;
         }
         return trainingSet;
      }
   }
}