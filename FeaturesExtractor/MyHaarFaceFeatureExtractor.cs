﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Biometrics.Face;
using BioLab.ImageProcessing;
using BioLab.Math.Geometry;
using BioLab.Common;
using BioLab.Math.LinearAlgebra;
using BioLab.DimensionalityReduction;

namespace FaceRecognition
{
  class MyHaarFaceFeatureExtractor : FaceFeatureExtractor
  {

    public MyHaarFaceFeatureExtractor()
    {
    }

    public MyHaarFaceFeatureExtractor(Image<byte> inputImage)
        : base(inputImage)
    {
    }

    public override void Run()
    {
      int featureA_Width = 5;
      double aspectRatioA = 1.5;
      int featureA_Height = (int)(featureA_Width * aspectRatioA);

      int featureB_Width = 5;
      double aspectRatioB = 1.0;
      int featureB_Height = (int)(featureB_Width * aspectRatioB);

      int featureC_Width = 5;
      double aspectRatioC = 1.0;
      int featureC_Height = (int)(featureC_Width * aspectRatioC);

      int featureD_Width = 5;
      double aspectRatioD = 1.5;
      int featureD_Height = (int)(featureD_Width * aspectRatioD);

      List<int> res = new List<int>();

      IntPoint2D p1, p2, p3, p4, p5, p6, p7, p8, p9;

      Image<int> integralImage = CalculateIntegralImage(InputImage);

      // Feature A
      for (int x = 0; x < InputImage.Width - (featureA_Width * 2) + 1; x++)
      {
        for (int y = 0; y < InputImage.Height - featureA_Height + 1; y++)
        {
          // Pos rect
          p1 = new IntPoint2D(x, y);
          p2 = new IntPoint2D(x + featureA_Width - 1, y);
          p3 = new IntPoint2D(x, y + featureA_Height - 1);
          p4 = new IntPoint2D(p2.X, p3.Y);
          p5 = new IntPoint2D(p4.X + featureA_Width - 1, p2.Y);
          p6 = new IntPoint2D(p5.X, p4.Y);
          res.Add(CalculateRectSum(integralImage, p1, p2, p3, p4) - CalculateRectSum(integralImage, p2, p5, p4, p6));
        }
      }

      // Feature B
      for (int x = 0; x < InputImage.Width - featureB_Width + 1; x++)
      {
        for (int y = 0; y < InputImage.Height - (featureB_Height * 2) + 1; y++)
        {
          p1 = new IntPoint2D(x, y);
          p2 = new IntPoint2D(x + featureB_Width - 1, y);
          p3 = new IntPoint2D(x, y + featureB_Height - 1);
          p4 = new IntPoint2D(p2.X, p3.Y);
          p5 = new IntPoint2D(x, p3.Y + featureB_Height - 1);
          p6 = new IntPoint2D(p2.X, p5.Y);
          res.Add(CalculateRectSum(integralImage, p3, p4, p5, p6) - CalculateRectSum(integralImage, p1, p2, p3, p4));
        }
      }

      // Feature C
      for (int x = 0; x < InputImage.Width - (featureC_Width * 3) + 1; x++)
      {
        for (int y = 0; y < InputImage.Height - featureC_Height + 1; y++)
        {
          p1 = new IntPoint2D(x, y);
          p2 = new IntPoint2D(x + featureC_Width - 1, y);
          p3 = new IntPoint2D(x, y + featureC_Height - 1);
          p4 = new IntPoint2D(p2.X, p3.Y);
          p5 = new IntPoint2D(p2.X + featureC_Width - 1, y);
          p6 = new IntPoint2D(p5.X, p4.Y);
          p7 = new IntPoint2D(p5.X + featureC_Width - 1, y);
          p8 = new IntPoint2D(p7.X, p4.Y);
          res.Add((CalculateRectSum(integralImage, p1, p2, p3, p4) + CalculateRectSum(integralImage, p5, p7, p6, p8)) - CalculateRectSum(integralImage, p2, p5, p4, p6));
        }
      }

      // Feature D
      for (int x = 0; x < InputImage.Width - (featureD_Width * 2) + 1; x++)
      {
        for (int y = 0; y < InputImage.Height - (featureD_Height * 2) + 1; y++)
        {
          p1 = new IntPoint2D(x, y);
          p2 = new IntPoint2D(x + featureD_Width - 1, y);
          p3 = new IntPoint2D(x, y + featureD_Height - 1);
          p4 = new IntPoint2D(p2.X, p3.Y);
          p5 = new IntPoint2D(p2.X + featureD_Width - 1, y);
          p6 = new IntPoint2D(p5.X, p4.Y);
          p7 = new IntPoint2D(x, p3.Y + featureD_Height - 1);
          p8 = new IntPoint2D(p2.X, p7.Y);
          p9 = new IntPoint2D(p5.X, p7.Y);
          res.Add((CalculateRectSum(integralImage, p1, p2, p3, p4) + CalculateRectSum(integralImage, p4, p6, p8, p9)) - (CalculateRectSum(integralImage, p2, p5, p4, p6) + CalculateRectSum(integralImage, p3, p4, p7, p8)));
        }
      }

      var val = new FeatureVector(res.Count());
      for (int i = 0; i < val.Features.Count; i++)
      {
        val[i] = res[i];
      }
    
      Result = val;
    }

    private int CalculateRectSum(Image<int> integralImage, IntPoint2D p1, IntPoint2D p2, IntPoint2D p3, IntPoint2D p4)
    {
      return integralImage[p4] + integralImage[p1] - integralImage[p2] - integralImage[p3];
    }

    private Image<int> CalculateIntegralImage(Image<byte> image)
    {
      var cursor = new ImageCursor(image);
      var IntegralImage = new Image<int>(image.Width, image.Height);

      do
      {
        IntegralImage[cursor.X, cursor.Y] = II(cursor.X, cursor.Y, image);
      } while (cursor.MoveNext());

      return IntegralImage;
    }

    private int II(int x, int y, Image<byte> Image)
    {
      if (x == -1)
        return 0;

      return II(x - 1, y, Image) + S(x, y, Image);
    }

    private int S(int x, int y, Image<byte> Image)
    {
      if (y == -1)
        return 0;

      return S(x, y - 1, Image) + Image[x, y];
    }
  }
}
