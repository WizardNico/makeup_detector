﻿using BioLab.Common;
using BioLab.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognition
{
  public class TextureFeatureExtractor
  {
    private Image<byte> InputImage;
    private Dictionary<int, double> Histogram;

    public TextureFeatureExtractor(Image<byte> Image)
    {
      InputImage = Image;
      Histogram = CreateHistorgam();
    }

    private static Dictionary<int, double> CreateHistorgam()
    {
      return new Dictionary<int, double>
      {
        { 0, 0 },
        { 1, 0 },
        { 2, 0 },
        { 3, 0 },
        { 4, 0 },
        { 6, 0 },
        { 7, 0 },
        { 8, 0 },
        { 12, 0 },
        { 14, 0 },
        { 15, 0 },
        { 16, 0 },
        { 24, 0 },
        { 28, 0 },
        { 30, 0 },
        { 31, 0 },
        { 32, 0 },
        { 48, 0 },
        { 56, 0 },
        { 60, 0 },
        { 62, 0 },
        { 63, 0 },
        { 64, 0 },
        { 96, 0 },
        { 112, 0 },
        { 120, 0 },
        { 124, 0 },
        { 126, 0 },
        { 127, 0 },
        { 128, 0 },
        { 129, 0 },
        { 131, 0 },
        { 135, 0 },
        { 143, 0 },
        { 159, 0 },
        { 191, 0 },
        { 192, 0 },
        { 193, 0 },
        { 195, 0 },
        { 199, 0 },
        { 207, 0 },
        { 223, 0 },
        { 224, 0 },
        { 225, 0 },
        { 227, 0 },
        { 231, 0 },
        { 239, 0 },
        { 240, 0 },
        { 241, 0 },
        { 243, 0 },
        { 247, 0 },
        { 248, 0 },
        { 249, 0 },
        { 251, 0 },
        { 252, 0 },
        { 253, 0 },
        { 254, 0 },
        { 255, 0 },
        { 256, 0 }
      };
    }

    public FeatureVector GetHistogramLBP()
    {
      ComputeLBP();
      return new FeatureVector(Histogram.Values.ToArray());
    }

    private void ComputeLBP()
    {
      var c = new ImageCursor(InputImage);
      do
      {
        if ((c.X != 0) && (c.Y != 0) && (c.X != InputImage.Width - 1) && (c.Y != InputImage.Height - 1))
        {
          ComputeArea(c);
        }
      } while (c.MoveNext());
    }

    private void ComputeArea(ImageCursor cursor)
    {
      int courrentValue;
      int transitionCount = 0;
      var centralValue = InputImage[cursor];
      string pattern = "";

      if (InputImage[cursor.NorthWest] >= centralValue)
      {
        courrentValue = 1;
        pattern = pattern + "1";
      }
      else
      {
        courrentValue = 0;
        pattern = pattern + "0";
      }

      if (InputImage[cursor.North] >= centralValue)
      {
        if (courrentValue == 0)
          transitionCount++;

        courrentValue = 1;
        pattern = pattern + "1";
      }
      else
      {
        if (courrentValue == 1)
          transitionCount++;

        courrentValue = 0;
        pattern = pattern + "0";
      }

      if (InputImage[cursor.NorthEast] >= centralValue)
      {
        if (courrentValue == 0)
          transitionCount++;

        courrentValue = 1;
        pattern = pattern + "1";
      }
      else
      {
        if (courrentValue == 1)
          transitionCount++;

        courrentValue = 0;
        pattern = pattern + "0";
      }

      if (InputImage[cursor.West] >= centralValue)
      {
        if (courrentValue == 0)
          transitionCount++;

        courrentValue = 1;
        pattern = pattern + "1";
      }
      else
      {
        if (courrentValue == 1)
          transitionCount++;

        courrentValue = 0;
        pattern = pattern + "0";
      }

      if (InputImage[cursor.East] >= centralValue)
      {
        if (courrentValue == 0)
          transitionCount++;

        courrentValue = 1;
        pattern = pattern + "1";
      }
      else
      {
        if (courrentValue == 1)
          transitionCount++;

        courrentValue = 0;
        pattern = pattern + "0";
      }

      if (InputImage[cursor.SouthWest] >= centralValue)
      {
        if (courrentValue == 0)
          transitionCount++;

        courrentValue = 1;
        pattern = pattern + "1";
      }
      else
      {
        if (courrentValue == 1)
          transitionCount++;

        courrentValue = 0;
        pattern = pattern + "0";
      }

      if (InputImage[cursor.South] >= centralValue)
      {
        if (courrentValue == 0)
          transitionCount++;

        courrentValue = 1;
        pattern = pattern + "1";
      }
      else
      {
        if (courrentValue == 1)
          transitionCount++;

        courrentValue = 0;
        pattern = pattern + "0";
      }

      if (InputImage[cursor.SouthEast] >= centralValue)
      {
        if (courrentValue == 0)
          transitionCount++;

        courrentValue = 1;
        pattern = pattern + "1";
      }
      else
      {
        if (courrentValue == 1)
          transitionCount++;

        courrentValue = 0;
        pattern = pattern + "0";
      }

      UpdateHistogram(transitionCount, pattern);
    }

    private void UpdateHistogram(int transitionCount, string pattern)
    {
      if (pattern[0] != pattern[pattern.Length - 1])
      {
        transitionCount++;
      }

      int n = Convert.ToInt32(pattern, 2);

      if (transitionCount <= 2)
      {
        Histogram[n] += 1;
      }
      else
      {
        Histogram[256] += 1;
      }
    }

  }
}
