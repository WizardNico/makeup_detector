﻿using BioLab.Common;
using BioLab.ImageProcessing;
using BioLab.ImageProcessing.Fourier;
using BioLab.Math.Geometry;
using BioLab.Math.LinearAlgebra;
using FaceRecognition.Models;
using FaceRecognition.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognition
{
   public class ShapeFeatureExtractor
   {
      private Image<byte> EdgeMap;
      private GradientCalculation GradientCalculator;
      private Image<byte> ScaledImage;
      private Image<byte> ScaledFilteredImage;
      private Image<ByteAngle> AnglesImage;
      private double[] Histogram { get; }

      public ShapeFeatureExtractor(RgbImage<byte> Image)
      {
         ScaledImage = new ByteAffineTransform(Image.ToByteImage(), 0.59, 0).Execute();
         ScaledFilteredImage = new ByteAffineTransform(FilterFactory.FilterImage(Image.ToByteImage()), 0.59, 0).Execute();
         EdgeMap = new CannyEdgeDetector(Image.ToByteImage()).Execute();
         GradientCalculator = new GradientCalculation(Image.ToByteImage(), GradientCalculationMethod.Sobel);
         GradientCalculator.Run();
         AnglesImage = GradientCalculator.CalculateByteAngles();
         Histogram = new double[36];
      }

      public ShapeFeatureExtractor()
      {
      }

      public FeatureVector GetEOH()
      {
         ComputeHistrogram();
         return new FeatureVector(Histogram.ToArray());
      }

      private void ComputeHistrogram()
      {
         var cEdgeMap = new ImageCursor(EdgeMap);
         do
         {
            //Se trovo un edge
            if (EdgeMap[cEdgeMap] == 255)
            {
               var img = GetRegion(cEdgeMap);
               var bin = GetMaxResponse(img);
               UpdateHistorgram(bin);
            }
         } while (cEdgeMap.MoveNext());
      }

      private int GetMaxResponse(Image<ByteAngle> Orientations)
      {
         int res;
         var map = new Dictionary<int, int>();

         foreach (var angle in Orientations)
         {
            var bin = GetBin(angle);

            if (map.ContainsKey(bin))
               map[bin] += 1;
            else
               map.Add(bin, 0);
         }

         //Ordino il bin con voti maggiori
         IOrderedEnumerable<KeyValuePair<int, int>> sortedMap = map.OrderByDescending(x => x.Value);

         //Controllo se ci sono più bin con la stessa risposta, allora andiamo a selezionare il più grande
         if (sortedMap.Count() > 1 && sortedMap.ElementAt(0).Value == sortedMap.ElementAt(1).Value)
         {
            var list = sortedMap.ToList().FindAll(x => x.Value == sortedMap.ElementAt(0).Value).OrderByDescending(x => x.Key);
            res = list.ElementAt(0).Key;
         }
         else
         {
            res = sortedMap.ElementAt(0).Key;
         }

         return res;
      }

      private int GetBin(ByteAngle Angle)
      {
         if (Angle.Degrees >= 0 && Angle.Degrees <= 10)
            return 10;

         if (Angle.Degrees >= 11 && Angle.Degrees <= 20)
            return 20;

         if (Angle.Degrees >= 21 && Angle.Degrees <= 30)
            return 30;

         if (Angle.Degrees >= 31 && Angle.Degrees <= 40)
            return 40;

         if (Angle.Degrees >= 41 && Angle.Degrees <= 50)
            return 50;

         if (Angle.Degrees >= 51 && Angle.Degrees <= 60)
            return 60;

         if (Angle.Degrees >= 61 && Angle.Degrees <= 70)
            return 70;

         if (Angle.Degrees >= 71 && Angle.Degrees <= 80)
            return 80;

         if (Angle.Degrees >= 81 && Angle.Degrees <= 90)
            return 90;

         if (Angle.Degrees >= 91 && Angle.Degrees <= 100)
            return 100;

         if (Angle.Degrees >= 101 && Angle.Degrees <= 110)
            return 110;

         if (Angle.Degrees >= 111 && Angle.Degrees <= 120)
            return 120;

         if (Angle.Degrees >= 121 && Angle.Degrees <= 130)
            return 130;

         if (Angle.Degrees >= 131 && Angle.Degrees <= 140)
            return 140;

         if (Angle.Degrees >= 141 && Angle.Degrees <= 150)
            return 150;

         if (Angle.Degrees >= 151 && Angle.Degrees <= 160)
            return 160;

         if (Angle.Degrees >= 161 && Angle.Degrees <= 170)
            return 170;

         if (Angle.Degrees >= 171 && Angle.Degrees <= 180)
            return 180;

         if (Angle.Degrees >= 181 && Angle.Degrees <= 190)
            return 190;

         if (Angle.Degrees >= 191 && Angle.Degrees <= 200)
            return 200;

         if (Angle.Degrees >= 201 && Angle.Degrees <= 210)
            return 210;

         if (Angle.Degrees >= 211 && Angle.Degrees <= 220)
            return 220;

         if (Angle.Degrees >= 221 && Angle.Degrees <= 230)
            return 230;

         if (Angle.Degrees >= 231 && Angle.Degrees <= 240)
            return 240;

         if (Angle.Degrees >= 241 && Angle.Degrees <= 250)
            return 250;

         if (Angle.Degrees >= 251 && Angle.Degrees <= 260)
            return 260;

         if (Angle.Degrees >= 261 && Angle.Degrees <= 270)
            return 270;

         if (Angle.Degrees >= 271 && Angle.Degrees <= 280)
            return 280;

         if (Angle.Degrees >= 281 && Angle.Degrees <= 290)
            return 290;

         if (Angle.Degrees >= 291 && Angle.Degrees <= 300)
            return 300;

         if (Angle.Degrees >= 301 && Angle.Degrees <= 310)
            return 310;

         if (Angle.Degrees >= 311 && Angle.Degrees <= 320)
            return 320;

         if (Angle.Degrees >= 321 && Angle.Degrees <= 330)
            return 330;

         if (Angle.Degrees >= 331 && Angle.Degrees <= 340)
            return 340;

         if (Angle.Degrees >= 341 && Angle.Degrees <= 350)
            return 350;

         if (Angle.Degrees >= 351 && Angle.Degrees <= 360)
            return 360;

         return 0;
      }

      private void UpdateHistorgram(int bin)
      {
         switch (bin)
         {
            case 10:
               Histogram[0]++;
               break;

            case 20:
               Histogram[1]++;
               break;

            case 30:
               Histogram[2]++;
               break;

            case 40:
               Histogram[3]++;
               break;

            case 50:
               Histogram[4]++;
               break;

            case 60:
               Histogram[5]++;
               break;

            case 70:
               Histogram[6]++;
               break;

            case 80:
               Histogram[7]++;
               break;

            case 90:
               Histogram[8]++;
               break;

            case 100:
               Histogram[9]++;
               break;

            case 110:
               Histogram[10]++;
               break;

            case 120:
               Histogram[11]++;
               break;

            case 130:
               Histogram[12]++;
               break;

            case 140:
               Histogram[13]++;
               break;

            case 150:
               Histogram[14]++;
               break;

            case 160:
               Histogram[15]++;
               break;

            case 170:
               Histogram[16]++;
               break;

            case 180:
               Histogram[17]++;
               break;

            case 190:
               Histogram[18]++;
               break;

            case 200:
               Histogram[19]++;
               break;

            case 210:
               Histogram[20]++;
               break;

            case 220:
               Histogram[21]++;
               break;

            case 230:
               Histogram[22]++;
               break;

            case 240:
               Histogram[23]++;
               break;

            case 250:
               Histogram[24]++;
               break;

            case 260:
               Histogram[25]++;
               break;

            case 270:
               Histogram[26]++;
               break;

            case 280:
               Histogram[27]++;
               break;

            case 290:
               Histogram[28]++;
               break;

            case 300:
               Histogram[29]++;
               break;

            case 310:
               Histogram[30]++;
               break;

            case 320:
               Histogram[31]++;
               break;

            case 330:
               Histogram[32]++;
               break;

            case 340:
               Histogram[33]++;
               break;

            case 350:
               Histogram[34]++;
               break;

            case 360:
               Histogram[35]++;
               break;
         }
      }

      private Image<ByteAngle> GetRegion(ImageCursor Cursor)
      {
         var region = new Image<ByteAngle>(3, 3)
         {
            [0, 0] = AnglesImage[Cursor.NorthWest],
            [0, 1] = AnglesImage[Cursor.North],
            [0, 2] = AnglesImage[Cursor.NorthEast],
            [1, 0] = AnglesImage[Cursor.West],
            [1, 1] = AnglesImage[Cursor],
            [1, 2] = AnglesImage[Cursor.East],
            [2, 0] = AnglesImage[Cursor.SouthWest],
            [2, 1] = AnglesImage[Cursor.South],
            [2, 2] = AnglesImage[Cursor.SouthEast]
         };

         return region;
      }

      public Image<double>[] GetGaborImages(ConvolutionFilter<double>[] Filters, bool Print)
      {
         var images = Filters
                     .AsParallel()
                     .AsOrdered()
                     .WithDegreeOfParallelism(Repository.MAX_THREAD_NUMBER / 2)
                     .Select(filter => ApplyGaborFilter(filter))
                     .ToArray();

         if (Print)
            printImages(images, ImageType.Gabor);

         return images;
      }

      private Image<double> ApplyGaborFilter(ConvolutionFilter<double> Filter)
      {
         //Effettua convoluzione
         var img = new ByteToDoubleConvolution(ScaledImage, Filter, 0).Execute();
         //Ritorna il risultato della convoluzione riscalandola alle dimensioni originali
         return new ByteAffineTransform(img.ToByteImage(), 1.85, 0).Execute().ToDoubleImage();
      }

      public Image<double>[] GetGISTImages(ConvolutionFilter<double>[] Filters, bool Print)
      {
         var images = Filters
                      .AsParallel()
                      .AsOrdered()
                      .WithDegreeOfParallelism(Repository.MAX_THREAD_NUMBER / 2)
                      .Select(filter => CalculateFourierTransform(filter))
                      .ToArray();

         if (Print)
            printImages(images, ImageType.Spectral);

         return images;
      }

      private Image<double> CalculateFourierTransform(ConvolutionFilter<double> Filter)
      {
         var gaborImage = new ByteAffineTransform(new ByteToDoubleConvolution(ScaledFilteredImage, Filter, 0).Execute().ToByteImage(), 1.85, 0).Execute().ToDoubleImage();

         var fft = new FftByte(ImageUtilities.Resize(gaborImage.ToByteImage(), 128, 128));
         fft.Run();

         return ImageUtilities.Resize(fft.Result.CalculateSpectrum(true).ToByteImage(), 130, 150).ToDoubleImage();
      }

      private void printImages(Image<double>[] images, ImageType Type)
      {
         for (int x = 0; x < images.Length; x++)
         {
            if (Type == ImageType.Gabor)
               images[x].SaveToFile(Repository.BASE_PATH + Repository.GABOR_IMAGES_PATH + "\\Gabor_Image " + x + ".jpg");
            else
               images[x].SaveToFile(Repository.BASE_PATH + Repository.SPECTRUM_PATH + "\\Spectrum_Image " + x + ".jpg");
         }
      }
   }
}
