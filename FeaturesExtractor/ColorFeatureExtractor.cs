﻿using BioLab.Common;
using BioLab.ImageProcessing;
using FaceRecognition.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FaceRecognition
{
   public class ColorFeatureExtractor
   {
      private List<Double> FeatureVector { get; set; }
      private List<Image<double>> HueList = null;
      private List<Image<double>> SaturationList = null;
      private List<Image<double>> ValueList = null;

      public ColorFeatureExtractor(HsvImage<double> Image, ImageType Type)
      {
         FaceTassellation(Image, Type);
      }

      public ColorFeatureExtractor(Image<double> Image, ImageType Type)
      {
         FaceTassellation(Image, Type);
      }

      private void FaceTassellation(Image<double> Image, ImageType Type)
      {
         FeatureVector = new List<double>();

         switch (Type)
         {
            case ImageType.Gabor:
               ExtractFeatures(Image);
               break;

            case ImageType.GIST:
               var blocks = TasselateChannel(Image, 4);
               ExtractMeanMomentFeatures(blocks);
               break;
         }
      }

      private void FaceTassellation(HsvImage<double> Image, ImageType Type)
      {
         FeatureVector = new List<double>();
         HueList = new List<Image<double>>();
         SaturationList = new List<Image<double>>();
         ValueList = new List<Image<double>>();

         switch (Type)
         {
            case ImageType.Face:
               Parallel.Invoke(() => HueList.AddRange(TasselateChannel(Image.HueChannel, 3)), () => SaturationList.AddRange(TasselateChannel(Image.SaturationChannel, 3)), () => ValueList.AddRange(TasselateChannel(Image.ValueChannel, 3)));
               break;

            case ImageType.ROI:
               Parallel.Invoke(() => HueList.AddRange(TasselateChannel(Image.HueChannel, 5)), () => SaturationList.AddRange(TasselateChannel(Image.SaturationChannel, 5)), () => ValueList.AddRange(TasselateChannel(Image.ValueChannel, 5)));
               break;
         }

         ExtractFeatures();
      }

      private void ExtractMeanMomentFeatures(List<Image<double>> BlocksList)
      {
         foreach (var block in BlocksList)
         {
            /*----MEAN----*/
            var N = block.Count();
            double m = 0.0;
            var cursor = new ImageCursor(block);
            do
            {
               m += (block[cursor] / N);
            } while (cursor.MoveNext());
            FeatureVector.Add(m);
         }
      }

      private void ExtractFeatures(Image<double> Image)
      {
         var N = Image.Count();

         /*---- SATURATION CHANNEL ----*/
         /*----MEAN----*/
         double m = 0.0;
         var cursor = new ImageCursor(Image);
         do
         {
            m += (Image[cursor] / N);
         } while (cursor.MoveNext());
         FeatureVector.Add(m);

         /*----DEVIAZIONE STANDARD----*/
         double x = 0.0;
         cursor.Restart();
         do
         {
            x += Math.Pow((Image[cursor] - m), 2);
         } while (cursor.MoveNext());
         x /= N;
         var stDev = Math.Sqrt(x);
         FeatureVector.Add(stDev);

         /*----SKEWNESS----*/
         double y = 0.0;
         cursor.Restart();
         do
         {
            y += Math.Pow((Image[cursor] - m), 3);
         } while (cursor.MoveNext());
         y /= N;
         FeatureVector.Add(CubicRoot(y));
      }

      private List<Image<double>> TasselateChannel(Image<double> Channel, int N)
      {
         var list = new List<Image<double>>();
         var blockHeight = Channel.Height / N;
         var blockWidth = Channel.Width / N;
         var step_x = Channel.Width / N;
         var step_y = Channel.Height / N;

         for (int y = 0; y < N; y++)
         {
            for (int x = 0; x < N; x++)
            {
               list.Add(Channel.GetSubImage(x * step_x, y * step_y, blockWidth, blockHeight).ToByteImage().ToDoubleImage());
            }
         }

         return list;
      }

      public FeatureVector GetFeatureVector() => new FeatureVector(FeatureVector.ToArray());

      public void ComputeChannel(List<Image<double>> Channel)
      {
         foreach (var block in Channel)
         {
            /*----MEAN----*/
            var N = block.Count();
            double m = 0.0;
            var cursor = new ImageCursor(block);
            do
            {
               m += (block[cursor] / N);
            } while (cursor.MoveNext());
            FeatureVector.Add(m);

            /*----DEVIAZIONE STANDARD----*/
            double x = 0.0;
            cursor.Restart();
            do
            {
               x += Math.Pow((block[cursor] - m), 2);
            } while (cursor.MoveNext());
            x /= N;
            FeatureVector.Add(Math.Sqrt(x));

            /*----SKEWNESS----*/
            double y = 0.0;
            cursor.Restart();
            do
            {
               y += Math.Pow((block[cursor] - m), 3);
            } while (cursor.MoveNext());
            y /= N;
            FeatureVector.Add(CubicRoot(y));
         }
      }

      public void ExtractFeatures()
      {
         ComputeChannel(SaturationList);
         ComputeChannel(HueList);
         ComputeChannel(ValueList);
      }

      private double CubicRoot(double num)
      {
         double skw = 0;
         if (num < 0)
         {
            skw = Math.Pow(-num, (1.0 / 3.0));
            return -skw;
         }
         else
         {
            skw = Math.Pow(num, (1.0 / 3.0));
            return skw;
         }
      }
   }
}
