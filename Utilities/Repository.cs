﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognition.Utilities
{
   public static class Repository
   {
      public static string BASE_PATH = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

      public static string TRAIN_MAKEUP_PATH = "\\Dataset_Esame\\YMU_Dataset\\MakeUp_Faces";
      public static string TRAIN_NO_MAKEUP_PATH = "\\Dataset_Esame\\YMU_Dataset\\No_MakeUp_Faces";
      public static string TRAINING_SET_MAKEUP_PATH = "\\Features\\Makeup_Faces.fvstxt";

      public static string VALIDATION_MAKEUP_PATH = "\\Dataset_Esame\\VMU\\Makeup";
      public static string VALIDATION_NO_MAKEUP_PATH = "\\Dataset_Esame\\VMU\\No_Makeup";

      public static string TEST_MAKEUP_PATH = "\\Dataset_Esame\\MIW_Dataset\\MakeUp";
      public static string TEST_NO_MAKEUP_PATH = "\\Dataset_Esame\\MIW_Dataset\\No_MakeUp";

      public static string TRAIN_LEFT_EYES_PATH = "\\DBEye\\Left";
      public static string TRAIN_RIGHT_EYES_PATH = "\\DBEye\\Right";
      public static string TRAIN_NO_EYES_PATH = "\\DBEye\\No_Eyes";
      public static string TRAINING_SET_EYES_PATH = "\\Features\\Eyes.fvstxt";
      public static string TRAIN_MOUTH_PATH = "\\LipsDb";
      public static string TRAIN_NO_MOUTH_PATH = "\\LipsDb\\non_mouth";
      public static string TRAINING_SET_MOUTH_PATH = "\\Features\\Mouths.fvstxt";
      public static string TRAIN_FACE_PATH = "\\DBFaceNonFace\\Training_Face\\indexFace.txt";
      public static string TRAIN_NO_FACE_PATH = "\\DBFaceNonFace\\Training_NonFace\\indexNonFace.txt";
      public static string TRAINING_SET_FACE_PATH = "\\Features\\Faces.fvstxt";

      public static string GABOR_IMAGES_PATH = "\\Gaborized_Faces";
      public static string SPECTRUM_PATH = "\\Spectrum_Images";

      public static int MAX_THREAD_NUMBER = Environment.ProcessorCount + 1;
   }
}
