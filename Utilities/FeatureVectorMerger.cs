﻿using BioLab.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognition.Utilities
{
   public static class FeatureVectorMerger
   {
      public static FeatureVector Merge(FeatureVector f1, FeatureVector f2, FeatureVector f3, FeatureVector f4) => new FeatureVector(f1.Concat(f2.Concat(f3.Concat(f4))).ToArray());

      public static FeatureVector Merge(FeatureVector f1, FeatureVector f2, FeatureVector f3) => new FeatureVector(f1.Concat(f2.Concat(f3)).ToArray());

      public static FeatureVector Merge(FeatureVector f1, FeatureVector f2) => new FeatureVector(f1.Concat(f2).ToArray());
   }
}
