﻿using BioLab.Classification.Supervised;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognition.Utilities
{
   public static class Logger
   {
      public static void Write(KernelType Kernel, int Error, int Cost, double Gamma, int Degree)
      {
         switch (Kernel)
         {
            case KernelType.RadialBasisFunction:
               Console.WriteLine("");
               Console.WriteLine("---------------------------------------------------------------------------------------------------");
               Console.WriteLine($"BEST ERROR={Error} | BEST COST={Cost} | BEST GAMMA={Gamma}");
               Console.WriteLine("---------------------------------------------------------------------------------------------------");
               Console.WriteLine("");
               break;

            case KernelType.Polynomial:
               Console.WriteLine("");
               Console.WriteLine("-------------------------------------------------------------------------------------------------------------------");
               Console.WriteLine($"BEST ERROR={Error} | BEST COST={Cost} | BEST DEGREE={Degree} | BEST GAMMA={Gamma}");
               Console.WriteLine("-------------------------------------------------------------------------------------------------------------------");
               Console.WriteLine("");
               break;

            case KernelType.Linear:
               Console.WriteLine("");
               Console.WriteLine("----------------------------------------------------------");
               Console.WriteLine($"BEST ERROR={Error} | BEST COST={Cost}");
               Console.WriteLine("----------------------------------------------------------");
               Console.WriteLine("");
               break;
         }
      }

      public static void Write(TimeSpan Time)
      {
         string elapsedTotalTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", Time.Hours, Time.Minutes, Time.Seconds, Time.Milliseconds / 10);
         Console.WriteLine("");
         Console.WriteLine("----------------------------------------");
         Console.WriteLine($"- ELAPSED TIME = {elapsedTotalTime}");
         Console.WriteLine("----------------------------------------");
         Console.WriteLine("");
      }

      public static void Write(int FalsePositive, int FalseNegative, int Cost, double Gamma, int Degree)
      {
         int toatalErorr = FalseNegative + FalsePositive;
         double accuracy = (154 - toatalErorr) / 154;
         double TP = 77 - FalseNegative;
         double classification_rate = TP / (TP + FalseNegative);
         double precision = TP / (TP + FalsePositive);

         Console.WriteLine("");
         Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------");
         Console.WriteLine($"- TOTAL ERROR= {toatalErorr}, ACCURACY= {accuracy}, PRECISION= {precision}, RECALL= {classification_rate}  | PARAMETERS: [COST = {Cost}, DEGREE = {Degree}, GAMMA = {Gamma}]");
         Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------");
         Console.WriteLine("");
      }

      public static void Write(int Error, int Cost, double Gamma, int Degree)
      {
         Console.WriteLine("");
         Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------");
         Console.WriteLine($"- ERROR ON TEST SET = {Error} | PARAMETERS = [COST = {Cost}, DEGREE = {Degree}, GAMMA = {Gamma}]");
         Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------");
         Console.WriteLine("");
      }
   }
}
