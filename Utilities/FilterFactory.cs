﻿using BioLab.ImageProcessing;
using BioLab.ImageProcessing.Fourier;
using System;
using System.IO;
using System.Linq;

namespace FaceRecognition.Utilities
{
   public static class FilterFactory
   {
      private static string BASE_PATH = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
      private static string GABOR_PATH = "\\Gabor_Filters";

      public static ConvolutionFilter<double>[] GaborFilterBank { get; set; }

      public static ConvolutionFilter<double>[] GetGaborFilterBank(int Orientations, int Scale, bool Print)
      {
         var f = Math.Sqrt(2);
         var Kmax = 0.35;
         int i = 0;
         var filters = new ConvolutionFilter<double>[Orientations * Scale];

         for (int v = 1; v <= Scale; v++)
         {
            for (int u = 0; u < Orientations; u++)
            {
               filters[i] = GaborFilter.Create((Kmax / (Math.Pow(f, v))), (u * Math.PI / 8), 64, 1, 1, false, Math.PI * 2);

               if (Print)
                  PrintFilter(filters[i], i);

               i++;
            }
         }
         return filters;
      }

      public static ConvolutionFilter<double> GetLowPassFilter()
      {
         double[] filterVal = new double[10 * 10];

         for (int i = 0; i < filterVal.Length; i++)
         {
            filterVal[i] = 1;
         }

         return new ConvolutionFilter<double>(filterVal, 150);
      }

      public static ConvolutionFilter<double> GetHighPassFilter()
      {
         return new ConvolutionFilter<double>(new double[] { -1, -1, -1, -1, 5, -1, -1, -1, -1 }, 1);
      }

      private static void PrintFilter(ConvolutionFilter<double> f, int i)
      {
         var img = new Image<double>(64, 64);
         for (int x = 0; x < 64; x++)
         {
            for (int y = 0; y < 64; y++)
            {
               img[x, y] = f[x, y];
            }
         }
         img.SaveToFile(BASE_PATH + GABOR_PATH + "\\Gabor_Filter_" + i + ".jpg");
      }


      public static  Image<byte> FilterImage(Image<byte> InputImage)
      {
         Image<byte> sharpImage = new FftHighPassFiltering(InputImage, 0.1).Execute();

         //^2
         for (int i = 0; i < sharpImage.Count(); i++)
         {
            sharpImage[i] = Math.Pow(sharpImage[i], 2).RoundAndClipToByte();
         }

         //Applico il passa basso
         Image<byte> denominator = new FftLowPassFiltering(sharpImage, 0.1).Execute();

         //Radice di 2
         for (int i = 0; i < denominator.Count(); i++)
         {
            denominator[i] = (20 + Math.Sqrt(denominator[i])).RoundAndClipToByte();
         }

         Image<byte> numerator = new FftHighPassFiltering(InputImage, 0.1).Execute();

         Image<byte> result = numerator.Clone();
         for (int i = 0; i < numerator.Count(); i++)
         {
            result[i] = (numerator[i] / denominator[i]).ClipToByte();
         }

         return result;
      }
   }
}
