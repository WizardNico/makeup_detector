﻿namespace FaceRecognition
{
    partial class FormFaceDetection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
         this.components = new System.ComponentModel.Container();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.lbl_complete = new System.Windows.Forms.Label();
         this.buttonTraining = new System.Windows.Forms.Button();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.lbl_Result = new System.Windows.Forms.Label();
         this.pnl_ROI_Detection = new System.Windows.Forms.Panel();
         this.btn_makeup = new System.Windows.Forms.Button();
         this.btn_mouth_roi = new System.Windows.Forms.Button();
         this.btn_eyes_roi = new System.Windows.Forms.Button();
         this.panel1 = new System.Windows.Forms.Panel();
         this.label5 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.textBoxScaleIncStep = new System.Windows.Forms.TextBox();
         this.buttonDetection = new System.Windows.Forms.Button();
         this.textBoxMaxScaleFactor = new System.Windows.Forms.TextBox();
         this.textBoxMinWindowSize = new System.Windows.Forms.TextBox();
         this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
         this.imageViewerInputImage = new BioLab.GUI.DataViewers.ImageViewer();
         this.imageViewerFaceCandidates = new BioLab.GUI.DataViewers.ImageViewer();
         this.fileListControlInputFiles = new BioLab.GUI.UserControls.FileListControl();
         this.btn_test = new System.Windows.Forms.Button();
         this.groupBox3 = new System.Windows.Forms.GroupBox();
         this.lbl_test = new System.Windows.Forms.Label();
         this.groupBox1.SuspendLayout();
         this.groupBox2.SuspendLayout();
         this.pnl_ROI_Detection.SuspendLayout();
         this.panel1.SuspendLayout();
         this.tableLayoutPanel1.SuspendLayout();
         this.groupBox3.SuspendLayout();
         this.SuspendLayout();
         // 
         // groupBox1
         // 
         this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.groupBox1.Controls.Add(this.lbl_complete);
         this.groupBox1.Controls.Add(this.buttonTraining);
         this.groupBox1.Location = new System.Drawing.Point(13, 13);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(450, 112);
         this.groupBox1.TabIndex = 0;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Train Detectors";
         // 
         // lbl_complete
         // 
         this.lbl_complete.AutoSize = true;
         this.lbl_complete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.lbl_complete.ForeColor = System.Drawing.Color.Lime;
         this.lbl_complete.Location = new System.Drawing.Point(146, 16);
         this.lbl_complete.Name = "lbl_complete";
         this.lbl_complete.Size = new System.Drawing.Size(137, 16);
         this.lbl_complete.TabIndex = 18;
         this.lbl_complete.Text = "Training complete!";
         this.lbl_complete.Visible = false;
         // 
         // buttonTraining
         // 
         this.buttonTraining.Location = new System.Drawing.Point(108, 50);
         this.buttonTraining.Name = "buttonTraining";
         this.buttonTraining.Size = new System.Drawing.Size(224, 42);
         this.buttonTraining.TabIndex = 16;
         this.buttonTraining.Text = "Train Face and Makeup Detector";
         this.buttonTraining.UseVisualStyleBackColor = true;
         this.buttonTraining.Click += new System.EventHandler(this.buttonTraining_Click);
         // 
         // groupBox2
         // 
         this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.groupBox2.Controls.Add(this.lbl_Result);
         this.groupBox2.Controls.Add(this.pnl_ROI_Detection);
         this.groupBox2.Controls.Add(this.panel1);
         this.groupBox2.Controls.Add(this.tableLayoutPanel1);
         this.groupBox2.Controls.Add(this.fileListControlInputFiles);
         this.groupBox2.Location = new System.Drawing.Point(12, 131);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(768, 494);
         this.groupBox2.TabIndex = 1;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "Detection";
         // 
         // lbl_Result
         // 
         this.lbl_Result.AutoSize = true;
         this.lbl_Result.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.lbl_Result.ForeColor = System.Drawing.Color.Lime;
         this.lbl_Result.Location = new System.Drawing.Point(509, 13);
         this.lbl_Result.Name = "lbl_Result";
         this.lbl_Result.Size = new System.Drawing.Size(0, 16);
         this.lbl_Result.TabIndex = 21;
         this.lbl_Result.Visible = false;
         // 
         // pnl_ROI_Detection
         // 
         this.pnl_ROI_Detection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.pnl_ROI_Detection.Controls.Add(this.btn_makeup);
         this.pnl_ROI_Detection.Controls.Add(this.btn_mouth_roi);
         this.pnl_ROI_Detection.Controls.Add(this.btn_eyes_roi);
         this.pnl_ROI_Detection.Location = new System.Drawing.Point(457, 418);
         this.pnl_ROI_Detection.Name = "pnl_ROI_Detection";
         this.pnl_ROI_Detection.Size = new System.Drawing.Size(305, 71);
         this.pnl_ROI_Detection.TabIndex = 17;
         // 
         // btn_makeup
         // 
         this.btn_makeup.Enabled = false;
         this.btn_makeup.Location = new System.Drawing.Point(10, 8);
         this.btn_makeup.Name = "btn_makeup";
         this.btn_makeup.Size = new System.Drawing.Size(137, 56);
         this.btn_makeup.TabIndex = 4;
         this.btn_makeup.Text = "Check Makeup presence";
         this.btn_makeup.UseVisualStyleBackColor = true;
         this.btn_makeup.Click += new System.EventHandler(this.btn_makeup_Click);
         // 
         // btn_mouth_roi
         // 
         this.btn_mouth_roi.Enabled = false;
         this.btn_mouth_roi.Location = new System.Drawing.Point(178, 40);
         this.btn_mouth_roi.Name = "btn_mouth_roi";
         this.btn_mouth_roi.Size = new System.Drawing.Size(124, 23);
         this.btn_mouth_roi.TabIndex = 3;
         this.btn_mouth_roi.Text = "Detect Mouth Region";
         this.btn_mouth_roi.UseVisualStyleBackColor = true;
         this.btn_mouth_roi.Click += new System.EventHandler(this.btn_mouth_roi_Click);
         // 
         // btn_eyes_roi
         // 
         this.btn_eyes_roi.Enabled = false;
         this.btn_eyes_roi.Location = new System.Drawing.Point(178, 8);
         this.btn_eyes_roi.Name = "btn_eyes_roi";
         this.btn_eyes_roi.Size = new System.Drawing.Size(124, 23);
         this.btn_eyes_roi.TabIndex = 2;
         this.btn_eyes_roi.Text = "Detect Eyes Region";
         this.btn_eyes_roi.UseVisualStyleBackColor = true;
         this.btn_eyes_roi.Click += new System.EventHandler(this.btn_eye_roi_Click);
         // 
         // panel1
         // 
         this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.panel1.Controls.Add(this.label5);
         this.panel1.Controls.Add(this.label4);
         this.panel1.Controls.Add(this.label3);
         this.panel1.Controls.Add(this.textBoxScaleIncStep);
         this.panel1.Controls.Add(this.buttonDetection);
         this.panel1.Controls.Add(this.textBoxMaxScaleFactor);
         this.panel1.Controls.Add(this.textBoxMinWindowSize);
         this.panel1.Location = new System.Drawing.Point(147, 418);
         this.panel1.Name = "panel1";
         this.panel1.Size = new System.Drawing.Size(304, 70);
         this.panel1.TabIndex = 15;
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Location = new System.Drawing.Point(4, 52);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(109, 13);
         this.label5.TabIndex = 13;
         this.label5.Text = "Scale increment step:";
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(4, 30);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(112, 13);
         this.label4.TabIndex = 12;
         this.label4.Text = "Maximum scale factor:";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(5, 8);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(111, 13);
         this.label3.TabIndex = 11;
         this.label3.Text = "Minimum window size:";
         // 
         // textBoxScaleIncStep
         // 
         this.textBoxScaleIncStep.Location = new System.Drawing.Point(135, 48);
         this.textBoxScaleIncStep.Name = "textBoxScaleIncStep";
         this.textBoxScaleIncStep.Size = new System.Drawing.Size(61, 20);
         this.textBoxScaleIncStep.TabIndex = 10;
         // 
         // buttonDetection
         // 
         this.buttonDetection.Location = new System.Drawing.Point(217, 7);
         this.buttonDetection.Name = "buttonDetection";
         this.buttonDetection.Size = new System.Drawing.Size(75, 56);
         this.buttonDetection.TabIndex = 1;
         this.buttonDetection.Text = "Detect Face";
         this.buttonDetection.UseVisualStyleBackColor = true;
         this.buttonDetection.Click += new System.EventHandler(this.ButtonDetection_Click);
         // 
         // textBoxMaxScaleFactor
         // 
         this.textBoxMaxScaleFactor.Location = new System.Drawing.Point(135, 26);
         this.textBoxMaxScaleFactor.Name = "textBoxMaxScaleFactor";
         this.textBoxMaxScaleFactor.Size = new System.Drawing.Size(61, 20);
         this.textBoxMaxScaleFactor.TabIndex = 9;
         // 
         // textBoxMinWindowSize
         // 
         this.textBoxMinWindowSize.Location = new System.Drawing.Point(135, 4);
         this.textBoxMinWindowSize.Name = "textBoxMinWindowSize";
         this.textBoxMinWindowSize.Size = new System.Drawing.Size(61, 20);
         this.textBoxMinWindowSize.TabIndex = 8;
         // 
         // tableLayoutPanel1
         // 
         this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.tableLayoutPanel1.ColumnCount = 2;
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
         this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
         this.tableLayoutPanel1.Controls.Add(this.imageViewerInputImage, 0, 0);
         this.tableLayoutPanel1.Controls.Add(this.imageViewerFaceCandidates, 1, 0);
         this.tableLayoutPanel1.Location = new System.Drawing.Point(147, 32);
         this.tableLayoutPanel1.Name = "tableLayoutPanel1";
         this.tableLayoutPanel1.RowCount = 1;
         this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
         this.tableLayoutPanel1.Size = new System.Drawing.Size(615, 382);
         this.tableLayoutPanel1.TabIndex = 14;
         // 
         // imageViewerInputImage
         // 
         this.imageViewerInputImage.BackColor = System.Drawing.SystemColors.Control;
         this.imageViewerInputImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.imageViewerInputImage.Dock = System.Windows.Forms.DockStyle.Fill;
         this.imageViewerInputImage.Location = new System.Drawing.Point(3, 3);
         this.imageViewerInputImage.Name = "imageViewerInputImage";
         this.imageViewerInputImage.Size = new System.Drawing.Size(301, 376);
         this.imageViewerInputImage.TabIndex = 2;
         // 
         // imageViewerFaceCandidates
         // 
         this.imageViewerFaceCandidates.BackColor = System.Drawing.SystemColors.Control;
         this.imageViewerFaceCandidates.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.imageViewerFaceCandidates.Dock = System.Windows.Forms.DockStyle.Fill;
         this.imageViewerFaceCandidates.Location = new System.Drawing.Point(310, 3);
         this.imageViewerFaceCandidates.Name = "imageViewerFaceCandidates";
         this.imageViewerFaceCandidates.Size = new System.Drawing.Size(302, 376);
         this.imageViewerFaceCandidates.TabIndex = 5;
         this.imageViewerFaceCandidates.Paint += new System.Windows.Forms.PaintEventHandler(this.imageViewerFaceCandidates_Paint);
         // 
         // fileListControlInputFiles
         // 
         this.fileListControlInputFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
         this.fileListControlInputFiles.FileFilter = "*.bmp;*.jpg;*.tif;*.tiff; *.gif";
         this.fileListControlInputFiles.Location = new System.Drawing.Point(3, 19);
         this.fileListControlInputFiles.MinimumSize = new System.Drawing.Size(129, 100);
         this.fileListControlInputFiles.Name = "fileListControlInputFiles";
         this.fileListControlInputFiles.Size = new System.Drawing.Size(138, 470);
         this.fileListControlInputFiles.TabIndex = 3;
         this.fileListControlInputFiles.SelectedFileNameChanged += new System.EventHandler(this.fileListControlInputFiles_SelectedFileNameChanged);
         // 
         // btn_test
         // 
         this.btn_test.Location = new System.Drawing.Point(45, 50);
         this.btn_test.Name = "btn_test";
         this.btn_test.Size = new System.Drawing.Size(216, 42);
         this.btn_test.TabIndex = 19;
         this.btn_test.Text = "Start Testing";
         this.btn_test.UseVisualStyleBackColor = true;
         this.btn_test.Click += new System.EventHandler(this.btn_test_Click);
         // 
         // groupBox3
         // 
         this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.groupBox3.Controls.Add(this.lbl_test);
         this.groupBox3.Controls.Add(this.btn_test);
         this.groupBox3.Location = new System.Drawing.Point(469, 13);
         this.groupBox3.Name = "groupBox3";
         this.groupBox3.Size = new System.Drawing.Size(311, 112);
         this.groupBox3.TabIndex = 20;
         this.groupBox3.TabStop = false;
         this.groupBox3.Text = "Test Performance of Makeup Detector";
         // 
         // lbl_test
         // 
         this.lbl_test.AutoSize = true;
         this.lbl_test.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.lbl_test.ForeColor = System.Drawing.Color.Lime;
         this.lbl_test.Location = new System.Drawing.Point(97, 26);
         this.lbl_test.Name = "lbl_test";
         this.lbl_test.Size = new System.Drawing.Size(111, 16);
         this.lbl_test.TabIndex = 20;
         this.lbl_test.Text = "Test complete!";
         this.lbl_test.Visible = false;
         // 
         // FormFaceDetection
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(792, 632);
         this.Controls.Add(this.groupBox3);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.groupBox1);
         this.MinimumSize = new System.Drawing.Size(758, 574);
         this.Name = "FormFaceDetection";
         this.Text = "Face detection";
         this.Load += new System.EventHandler(this.FormFaceDetection_Load);
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.groupBox2.ResumeLayout(false);
         this.groupBox2.PerformLayout();
         this.pnl_ROI_Detection.ResumeLayout(false);
         this.panel1.ResumeLayout(false);
         this.panel1.PerformLayout();
         this.tableLayoutPanel1.ResumeLayout(false);
         this.groupBox3.ResumeLayout(false);
         this.groupBox3.PerformLayout();
         this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonTraining;
        private BioLab.GUI.UserControls.FileListControl fileListControlInputFiles;
        private System.Windows.Forms.Button buttonDetection;
        private BioLab.GUI.DataViewers.ImageViewer imageViewerFaceCandidates;
        private BioLab.GUI.DataViewers.ImageViewer imageViewerInputImage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxScaleIncStep;
        private System.Windows.Forms.TextBox textBoxMaxScaleFactor;
        private System.Windows.Forms.TextBox textBoxMinWindowSize;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label lbl_complete;
    private System.Windows.Forms.Panel pnl_ROI_Detection;
    private System.Windows.Forms.Button btn_eyes_roi;
    private System.Windows.Forms.Button btn_mouth_roi;
    private System.Windows.Forms.Button btn_makeup;
    private System.Windows.Forms.Button btn_test;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.Label lbl_test;
      private System.Windows.Forms.Label lbl_Result;
   }
}

