﻿using BioLab.ImageProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceRecognition
{
  public class HsvImage<T>
  {
    public Image<double> HueChannel { get; set; }
    public Image<double> ValueChannel { get; set; }
    public Image<double> SaturationChannel { get; set; }

    private Double Max { get; set; }
    private Double Min { get; set; }

    public HsvImage(RgbImage<byte> image)
    {
      InitializeVariables(image);
      ComputeHSV(image);
    }

    private void InitializeVariables(RgbImage<byte> image)
    {
      //Inizializzo le tre immagini rappresentanti i 3 canali
      HueChannel = new Image<double>(image.Width, image.Height);
      SaturationChannel = new Image<double>(image.Width, image.Height);
      ValueChannel = new Image<double>(image.Width, image.Height);
    }

    private void ComputeHSV(RgbImage<byte> image)
    {
      var c = new ImageCursor(image.RedChannel);
      do
      {
        //Cambiamo il range da [0,255] a [0,1] per i 3 canali
        var red_pixel = (double)image.RedChannel[c] / 255;
        var green_pixel = (double)image.GreenChannel[c] / 255;
        var blue_pixel = (double)image.BlueChannel[c] / 255;

        //trovo il massimo e il minimo
        Max = Math.Max(red_pixel, Math.Max(green_pixel, blue_pixel));
        Min = Math.Min(red_pixel, Math.Min(green_pixel, blue_pixel));

        var d = (red_pixel == Min) ? green_pixel - blue_pixel : ((blue_pixel == Min) ? red_pixel - green_pixel : blue_pixel - red_pixel);
        var h = (red_pixel == Min) ? 3 : ((blue_pixel == Min) ? 1 : 5);

        HueChannel[c] = 60 * (h - d / (Max - Min));
        SaturationChannel[c] = (Max - Min) / Max;
        ValueChannel[c] = Max;
      } while (c.MoveNext());
    }
  }
}
